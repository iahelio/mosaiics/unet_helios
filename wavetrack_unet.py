import numpy as np
from unet_segmentation import t_segmentation
import sys
import os
import random
import copy
from keras import backend 
from tensorflow.keras import backend as K
from scipy.stats import norm


def main():
    
    K.clear_session() 
    
    print(' Tensorflow Unet training routine for Wavetrack output. Written by Oleg Stepanyuk. \n')
  
    if ((len(sys.argv)) < 6):
     
        print('Syntax:') 
        print('> python3 -train <-bd/-rd/-nd> <model_id> <train_data_numpy_path> <masks_numpy_path> <base_bath> <max_files> <epochs>  <try testdata y/n> <test_data_path>  <test_masks_path> ')
        #                    1       2         3                 4                   5                6          7           8              9                                                               
        print('> python3 -train_engineer <-bd/-rd/-nd> <model_id> <train_data_numpy_path> <masks_numpy_path> <base_path> <max_files> <epochs> <try_testdata y/n> <angle> <angle_itt> <sigma> <sigma_inc> <test_data_path>  <test_masks_path> ') 
        #                    1                2         3                 4                  5                  6           7         8            9            10         11        12       13                                  
        print('> python3 -eval <model_id> <-bd/-rd/-nd> <test_data_path> <masks_numpy_path> <max_files>')
        #                   1       2         3              4               5                6
        print('> python3 -predict <model_id> <-bd/-rd/-nd> <test_data_path> <masks_numpy_path> <max_files>' )
        #                   1        2           3            4                 5               6                 
        
        sys.exit() 


    elif ( (sys.argv[1]=='-train') or (sys.argv[1]=='-train_engineer')): 
            

        try:
       
           
            data_files_arr = os.listdir(sys.argv[4])
            data_files_n = len(data_files_arr)
            masks_files_arr = os.listdir(sys.argv[5])
            masks_files_n = len(masks_files_arr)
            max_files_n=int(sys.argv[7])
           
        
        except: 
        
            print('Error: wrong train_data or masks directory')  
            print(sys.argv[4])
            print(sys.argv[5])
            sys.exit() 
        
    
        if (masks_files_n!=data_files_n): 
           print('Error: wrong training set: unequal amount of training data and mask files !')  
           sys.exit() 


        test_data_loaded=False


        if (sys.argv[9]=='y'): 
           
            
            #try:
               
                if (sys.argv[1]=='-train_engineer'): 
                    test_data_path = sys.argv[14] #input("Input test raw/bd/rd test data path:\n")
                    test_masks_path = sys.argv[15] #input("Input test raw/bd/rd test data path:\n")
                else: 
                    test_data_path = sys.argv[10] #input("Input test raw/bd/rd test data path:\n")
                    test_masks_path = sys.argv[11] #input("Input test raw/bd/rd test data path:\n")
        
            
                print('Loading test data....') 

            
        
                data_files_arr_test = os.listdir(test_data_path)   
                data_files_n_test = len(data_files_arr)
               
                masks_files_arr_test = os.listdir(test_masks_path)
                masks_files_n_test = len(masks_files_arr)
                max_files_n_test=int(sys.argv[7])
               
                if (masks_files_n_test!=data_files_n_test): 
                    print('Error: wrong test set: unequal amount of data and mask files: ')
                    print(str(masks_files_n_test)+','+ str(data_files_n_test))  
                    sys.exit() 
               
                ml_test = t_segmentation(data_files_n_test,max_files_n_test)  
                ml_test.max_files_n = max_files_n_test 
                ml_test.diff_method = '-nd' 
                ml_test.load_np_trainset(test_data_path ,test_masks_path, ' ')
                #ml_test.model_id = int(sys.argv[3])
                test_data_loaded=True
                verify_log_testdata_file = open(os.getcwd()+ml_test.verify_log_testdata_fname+'_'+str(sys.argv[3])+'.txt', 'w')  
            
            
            #except: 
        
            #    print('Error loading test data')  
            #    sys.exit() 

    
        ml_seg = t_segmentation(data_files_n,max_files_n)        
        ml_seg.diff_method = sys.argv[2]
        ml_seg.model_id = int(sys.argv[3])
        ml_seg.max_files_n = int(sys.argv[7]) 
        ml_seg.epochs_n = int(sys.argv[8])
        
    
        if (sys.argv[1]=='-train_engineer'):
            
            
            verify_log_self_file = open(os.getcwd()+ml_seg.verify_log_self_fname+'_'+str(ml_seg.model_id)+'.txt', 'w')

            angle_k = int(sys.argv[10])
            angle_inc = int(sys.argv[11])
            dyn_sigma_k =  float(sys.argv[12])
            dyn_sigma_inc = float(sys.argv[13])
     
            angle_itt_n= int( (360-angle_k)/angle_inc ) 

            if ( (dyn_sigma_k==0) or (dyn_sigma_inc==0) ):
                dyn_sigma_k= 0.01
                dyn_sigma_inc = 0.01 
                do_dyn_shift = False
                engineer_itt = angle_itt_n
            else: 
                do_dyn_shift = True 
                dyn_sigma_itt_n = int(dyn_sigma_k/dyn_sigma_inc)
                engineer_itt = angle_itt_n*dyn_sigma_itt_n    
            
            print('Total feature engineering itterations: '+str(engineer_itt))
            ml_seg.epochs_n = int(ml_seg.epochs_n/engineer_itt) 

            print('Epochs per itteration engineering itteration:')
            print(ml_seg.epochs_n)
            
        
        loaded = ml_seg.model_load(ml_seg.model_id)
        
        if (loaded > 0):
            print('Mooving on with pre-trained model ID: '+str(ml_seg.model_id))

        else: 
            print('Model ID not found. Initializing new U-NET model')    
            ml_seg.use_unet() 
        
        ml_seg.load_np_trainset(sys.argv[4], sys.argv[5], sys.argv[6])
        
        #print('Debug, correspondence between loaded masks:')
        #ml_seg.predictions = np.copy(ml_seg.data)
        #corr_orig=ml_seg.compare_predictions_w_ctl(ml_seg)
        #print('Correspondence:' +str(corr_orig))
        #sys.exit()
     
        ml_seg.data_2_trainset()
        ml_seg.model_train_straight()
        ml_seg.model_save()
        print('Done training model with Id:'+str(ml_seg.model_id))

        print('Running predictions on the original data:')
        ml_seg.model_predict() 
        ml_seg.save_predictions()
        corr_orig=ml_seg.compare_predictions_w_ctl(ml_seg)
        print('Correspondence:' +str(corr_orig))

        if (sys.argv[9]=='y') and (test_data_loaded==True):
  
           print('Running predictions on the test_data:')
           ml_test.model_load(ml_seg.model_id)
           ml_test.model_predict()
           corr_ctl=ml_test.compare_predictions_w_ctl(ml_test)
           print('Correspondence:' +str(corr_ctl))


        if (sys.argv[1]=='-train_engineer'):

            print('Manufacturing objects. Rotating data...')
          
            for angle in range(angle_k,360,angle_inc):  
               for dyn_sigma in range (0,int(dyn_sigma_k*100),int(dyn_sigma_inc*100)): 

                    angle_rand=random.randrange(angle_k, 360, angle_inc)
                    dyn_sigma_rand=random.randrange(0,int(dyn_sigma_k*100), int(dyn_sigma_inc*100))/100

                    print('Random Angle:'+str(angle_rand)) 
                    print('Random Sigma dynrange:' +str(dyn_sigma_rand))
                    
                    K.clear_session() 
                    
                    ml_seg_e = t_segmentation(data_files_n,max_files_n)    
                    
                    #ml_seg_e = copy.copy(ml_seg)
                    #del ml_seg_e.model
                    #del ml_seg_e.train_dataset

                    ml_seg_e.files_n=ml_seg.files_n 
                    ml_seg_e.max_files_n = ml_seg.max_files_n
                    ml_seg_e.data_frames_n = ml_seg.data_frames_n
        
                    ml_seg_e.base_max_files_n = ml_seg.base_max_files_n 
                    ml_seg_e.base_files_n = ml_seg.base_files_n  
                    ml_seg_e.model_id = ml_seg.model_id 

                    ml_seg_e.channels_n =  ml_seg.channels_n  # Grayscale = 1, RGB = 3
                    ml_seg_e.size_x = ml_seg.size_x 
                    ml_seg_e.size_y = ml_seg.size_y
       
                    ml_seg_e.diff_method = ml_seg.diff_method 
                    ml_seg_e.norm_rd_by_itself = ml_seg.norm_rd_by_itself 
        
                    ml_seg_e.do_normalize_data =  ml_seg.do_normalize_data
                    ml_seg_e.do_normalize_masks =   ml_seg.do_normalize_masks 

                    ml_seg_e.epochs_n = ml_seg.epochs_n 
                    ml_seg_e.batch_size = ml_seg.batch_size
                    ml_seg_e.do_shuffle =  ml_seg.do_shuffle
                    ml_seg_e.shuffle_buffer_size =  ml_seg.shuffle_buffer_size
                    #ml_seg_e.validation_split =  ml_seg.validation_split
                    ml_seg_e.verbose = ml_seg.verbose

                    ml_seg_e.model_load(ml_seg.model_id)                
                    ml_seg_e.masks = copy.deepcopy(ml_seg.masks)
                    ml_seg_e.data = copy.deepcopy(ml_seg.data)
                    
                    ml_seg_e.masks_n = ml_seg.masks_n                    
            
                    #ml_seg_e.masks_frames_n = ml_seg.masks_frames_n    
                     
                    if ( (angle_rand>0)): #and (angle_inc>0)):
                        ml_seg_e.rotate_data_masks(angle_rand) 
                   
                    if (do_dyn_shift==True):
                    #if ( (dyn_sigma_k>0) and (dyn_sigma_inc>0)):
                        ml_seg_e.shift_dynrange_masks(dyn_sigma_rand)                  
 
                    ml_seg_e.data_2_trainset()
                    ml_seg_e.model_train_straight()
                    ml_seg_e.save_control(angle_rand, dyn_sigma_rand)
                    ml_seg_e.model_save_interim(angle_rand,dyn_sigma_rand)
                    ml_seg_e.model_save()
                
                    print('Done training itteration:'+str(engineer_itt))
                    
                    ml_seg_et = t_segmentation(data_files_n,max_files_n)    
                    ml_seg_et = copy.copy(ml_seg)
                    del ml_seg_et.model
                    ml_seg_et.model_load(ml_seg.model_id)                
                    ml_seg_et.masks = copy.deepcopy(ml_seg.masks)
                    ml_seg_et.data = copy.deepcopy(ml_seg.data)

                    del ml_seg_e 

                    print('Running predictions on the original non engineered data:')
                    ml_seg_et.model_predict() 
                    loss,acc=ml_seg_et.model_evaluate()
                    corr_orig=ml_seg_et.compare_predictions_w_ctl(ml_seg_et)

                    print('Correspondence with the original non engineered set:' +str(corr_orig))

                    line='model_id:'+str(ml_seg_et.model_id)+' angle: '+str(angle_rand)+' sigma_shift: '+str(dyn_sigma_rand)+' corr: '+str(corr_orig)+' loss: '+str(loss) +' acc: ' + str(acc)+  '\n'
                    verify_log_self_file.write(line)
                    

                    del ml_seg_et
                   
                    if (sys.argv[9]=='y') and (test_data_loaded==True):
                        
                        print('Running predictions on the test_data')
                        del ml_test.model 
                        ml_test.model_load_interim(ml_seg.model_id, angle_rand,dyn_sigma_rand)
                        ml_test.model_predict()
                        #loss,acc=ml_test.model_evaluate()
                        ml_test.model_id = int(ml_seg.model_id)
                        corr_ctl=ml_test.compare_predictions_w_ctl(ml_test) 
                        print('Correspondence with the control set:' +str(corr_ctl))
                        line='model_id:'+str(ml_test.model_id)+' angle: '+str(angle_rand)+' sigma_shift: '+str(dyn_sigma_rand)+' corr: '+str(corr_ctl)+'\n' #' loss: '+str(loss) +' acc: ' +str(acc)+'\n'
                        verify_log_testdata_file.write(line)
                    
                


    elif ( ( sys.argv[1]=='-eval' ) or ( sys.argv[1]=='-predict')): 

        try:
       
            data_files_arr = os.listdir(sys.argv[4])
            data_files_n = len(data_files_arr)
            max_files_n=int(sys.argv[6])
            
        except: 
        
            print('Error: unable to read data from the folder')  
            sys.exit() 

        
        ml_seg = t_segmentation(data_files_n,max_files_n)
        ml_seg.diff_method = sys.argv[3]
        ml_seg.model_load(int(sys.argv[2]))
        ml_seg.model_id = int(sys.argv[2])
        ml_seg.max_files_n = int(sys.argv[6])
        ml_seg.load_np_trainset(sys.argv[4],sys.argv[5],'none')


        if (sys.argv[1]=='-predict'):
    
            ml_seg.model_predict() 
            ml_seg.save_predictions()
            here

        elif (sys.argv[1]=='-eval'):
            
            ml_seg.model_evaluate() 




if __name__ == "__main__":
    main()




