
import numpy as np

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, concatenate, Conv2D, MaxPooling2D, UpSampling2D, Reshape, Dropout
from tensorflow.keras import layers


from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD
import sys

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Flatten, Dense
from t_struct_eng import t_struct_eng

import os

import tensorflow as tf
import matplotlib.pyplot as plt
import math
from scipy.stats import norm


class t_segmentation(object):


#Define the neural network
    def get_unet(self, n_ch,patch_height,patch_width):
        inputs = Input(shape=(n_ch,patch_height,patch_width))
        #inputs = Input(shape=(patch_height,patch_width,n_ch))
        
        conv1 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format='channels_first')(inputs)
        conv1 = Dropout(0.2)(conv1)
        conv1 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format='channels_first')(conv1)
        pool1 = MaxPooling2D((2, 2))(conv1)
        #
        conv2 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format='channels_first')(pool1)
        conv2 = Dropout(0.2)(conv2)
        conv2 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format='channels_first')(conv2)
        pool2 = MaxPooling2D((2, 2))(conv2)
        #
        conv3 = Conv2D(128, (3, 3), activation='relu', padding='same',data_format='channels_first')(pool2)
        conv3 = Dropout(0.2)(conv3)
        conv3 = Conv2D(128, (3, 3), activation='relu', padding='same',data_format='channels_first')(conv3)

        up1 = UpSampling2D(size=(2, 2))(conv3)
        up1 = concatenate([conv2,up1],axis=1)
        conv4 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format='channels_first')(up1)
        conv4 = Dropout(0.2)(conv4)
        conv4 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format='channels_first')(conv4)
        #
        up2 = UpSampling2D(size=(2, 2))(conv4)
        up2 = concatenate([conv1,up2], axis=1)
        conv5 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format='channels_first')(up2)
        conv5 = Dropout(0.2)(conv5)
        conv5 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format='channels_first')(conv5)
        #
        conv6 = Conv2D(2, (1, 1), activation='relu',padding='same',data_format='channels_first')(conv5)
        conv6 = layers.Reshape((2,patch_height*patch_width))(conv6)
        conv6 = layers.Permute((2,1))(conv6)
        ############
        conv7 = layers.Activation('softmax')(conv6)

        model = Model(inputs=inputs, outputs=conv7)

        # sgd = SGD(lr=0.01, decay=1e-6, momentum=0.3, nesterov=False)
        model.compile(optimizer='sgd', loss='categorical_crossentropy',metrics=['accuracy'])

        return model


        #Define the neural network gnet
        #you need change function call "get_unet" to "get_gnet" in line 166 before use this network
    
    def use_unet(self):
        print('Using u-net model topology')
        self.model = self.get_unet(self.channels_n, self.size_x , self.size_y)

     

    def get_gnet(self,n_ch,patch_height,patch_width):
    
        inputs = Input((n_ch, patch_height, patch_width))
        conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(inputs)
        conv1 = Dropout(0.2)(conv1)
        conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
        up1 = UpSampling2D(size=(2, 2))(conv1)
        #
        conv2 = Convolution2D(16, 3, 3, activation='relu', border_mode='same')(up1)
        conv2 = Dropout(0.2)(conv2)
        conv2 = Convolution2D(16, 3, 3, activation='relu', border_mode='same')(conv2)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv2)
        #
        conv3 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(pool1)
        conv3 = Dropout(0.2)(conv3)
        conv3 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv3)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv3)
        #
        conv4 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(pool2)
        conv4 = Dropout(0.2)(conv4)
        conv4 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv4)
        pool3 = MaxPooling2D(pool_size=(2, 2))(conv4)
        #
        conv5 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(pool3)
        conv5 = Dropout(0.2)(conv5)
        conv5 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv5)
        #
        up2 = merge([UpSampling2D(size=(2, 2))(conv5), conv4], mode='concat', concat_axis=1)
        conv6 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up2)
        conv6 = Dropout(0.2)(conv6)
        conv6 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv6)
        #
        up3 = merge([UpSampling2D(size=(2, 2))(conv6), conv3], mode='concat', concat_axis=1)
        conv7 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up3)
        conv7 = Dropout(0.2)(conv7)
        conv7 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv7)
        #
        up4 = merge([UpSampling2D(size=(2, 2))(conv7), conv2], mode='concat', concat_axis=1)
        conv8 = Convolution2D(16, 3, 3, activation='relu', border_mode='same')(up4)
        conv8 = Dropout(0.2)(conv8)
        conv8 = Convolution2D(16, 3, 3, activation='relu', border_mode='same')(conv8)
        #
        pool4 = MaxPooling2D(pool_size=(2, 2))(conv8)
        conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(pool4)
        conv9 = Dropout(0.2)(conv9)
        conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)
        #
        conv10 = Convolution2D(2, 1, 1, activation='relu', border_mode='same')(conv9)
        conv10 = core.Reshape((2,patch_height*patch_width))(conv10)
        conv10 = core.Permute((2,1))(conv10)
        ############
        conv10 = core.Activation('softmax')(conv10)

        model = Model(input=inputs, output=conv10)

        # sgd = SGD(lr=0.01, decay=1e-6, momentum=0.3, nesterov=False)
        model.compile(optimizer='sgd', loss='categorical_crossentropy',metrics=['accuracy'])

        self.model=model
        return model

    

    def get_unet_2(self, image_rows, image_cols):
        inputs = Input((1, image_rows, image_cols))
        conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(inputs)
        conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

        conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(pool1)
        conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

        conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(pool2)
        conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv3)
        pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

        conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(pool3)
        conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv4)
        pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

        conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(pool4)
        conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(conv5)


        up6 = merge([UpSampling2D(size=(2, 2))(conv5), conv4], mode='concat', concat_axis=1)
        conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(up6)
        conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv6)

        up7 = merge([UpSampling2D(size=(2, 2))(conv6), conv3], mode='concat', concat_axis=1)
        conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(up7)
        conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv7)

        up8 = merge([UpSampling2D(size=(2, 2))(conv7), conv2], mode='concat', concat_axis=1)
        conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up8)
        conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)

        up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1], mode='concat', concat_axis=1)
        conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up9)
        conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)

        conv10 = Convolution2D(1, 1, 1, activation='sigmoid')(conv9)

        model = Model(input=inputs, output=conv10)

        return model 




    
    def normalize_0_1(self,data_arr):   

        # here: shift minus to 0 ?  
        #SHIFT LOWER LImIT TO 0 !!!
        
        data_arr_no_nan = np.nan_to_num(data_arr)
        data_arr_min = np.amin(data_arr_no_nan) 
        data_arr_from_zero = np.subtract(data_arr_no_nan,data_arr_min)
        data_arr_max = np.amax(data_arr_from_zero) 
        data_arr_adj_f = np.multiply(data_arr_from_zero,1/data_arr_max)
        return data_arr_adj_f.astype(np.float)

  

    def load_np_data(self, path, do_normalize):
       
        files_arr = os.listdir(path)
        files_arr.sort()
         
        print('Loading numpy data from:'+path,'...')
        files_n = len(files_arr)
        
        '''
        if (files_n!=self.files_n):
            print('Warning:'+path+' files count is different from pre-initianalized:')
            print('files_n: ' +str(files_n)+', self.files_n: '+str(self.files_n))
            sys.exit()
        ''' 
        
        data_frames_n = min(self.max_files_n, files_n) 
        
        full_data=np.empty([data_frames_n,self.channels_n,self.size_x,self.size_y],dtype=float)
    

        for file_i in range(1,files_n-1):
            print('Reading numpy data from:' +files_arr[file_i])            
            
            if (do_normalize):
                curr_data=self.normalize_0_1(np.load(path+'/'+files_arr[file_i]))
                print('Data normalization, maximum value:'+ str(np.max(curr_data)))

            else: 
                curr_data=np.load(path+'/'+files_arr[file_i])
                print('No data normalization, maximum value:'+ str(np.max(curr_data)))
            
            
            '''
            print(np.shape(curr_data))
            anykey=input()

            if (np.ndim(curr_data)!=self.channels_n):
                print('Error, wrong input data dimensions. File: '+str(np.ndim(curr_data))+'d, Pre-initialized: '+str(self.channels_n)+'d')
                sys.exit()

            if (np.ndim(curr_data)==2):
                print('2D array, assuming grayscale...')
                full_data[file_i,1]=np.copy(curr_data)
               

            elif (np.ndim(curr_data)==3):         
                print('3D array, assuming RGB...')
                full_data[file_i]=np.copy(curr_data)  
            '''
            
            #reshaping for my standard tensors
            #imgs = np.transpose(imgs,(0,3,1,2))
            
        
            full_data[file_i,0,:,:]=np.copy(curr_data)
               

        print('Red '+str(file_i)+' files')
        return full_data, files_n
   
  

    
    def rotate_data_masks(self, angle):

        for i in range(0, self.masks_n):
            
            struct_eng_mask = t_struct_eng() 
            struct_eng_mask.data = np.empty([self.size_x,self.size_y],dtype=float)
            struct_eng_mask.data=self.masks[i,0,:,:]
            struct_eng_mask.rotate_data_scipy(angle,'False')   
            self.masks[i]=struct_eng_mask.data
            del struct_eng_mask

        for i in range(0, self.data_frames_n):
            
            struct_eng_data = t_struct_eng() 
            struct_eng_data.data=np.empty([self.size_x,self.size_y], dtype=float)
            struct_eng_data.data=self.data[i,0,:,:]
            struct_eng_data.rotate_data_scipy(angle,'True')
            self.data[i]=struct_eng_data.data
            del struct_eng_data
    
    

    def shift_dynrange_masks(self, sigma_value):

        k = 1
        n = self.data.shape[0]
        
        for i in range(k,n):
                
                fitted_mean,fitted_std=norm.fit(self.data[i])
                abs_adj = fitted_std*sigma_value
                inc_mask = np.multiply(self.masks[i],abs_adj)
                self.data[i] = np.add(self.data[i], inc_mask)            
                 
    


    def masks_Unet(self, masks):
     
        assert (len(masks.shape)==4)  #4D arrays
        assert (masks.shape[1]==1 )  #check the channel is 1
     
        im_h = masks.shape[2]
        im_w = masks.shape[3]
     
        masks = np.reshape(masks,(masks.shape[0],im_h*im_w))
        new_masks = np.empty((masks.shape[0],im_h*im_w,2))
     
        for i in range(masks.shape[0]):
            for j in range(im_h*im_w):
     
                if  masks[i,j] == 0:
                    #print('true')
                    new_masks[i,j,0]=1
                    new_masks[i,j,1]=0
     
                else:
                    new_masks[i,j,0]=0
                    new_masks[i,j,1]=1
     
        return new_masks


     
    def pred_to_imgs(self, pred, patch_height, patch_width, mode="original"):
        
        thrhold = (np.amax(pred)-np.amin(pred))/2
        assert (len(pred.shape)==3)  #3D array: (Npatches,height*width,2)
        assert (pred.shape[2]==2 )  #check the classes are 2
        #pred_images = np.empty((pred.shape[0],pred.shape[1]))  #(Npatches,height*width)
        pred_images = np.zeros((pred.shape[0],pred.shape[1]))  #(Npatches,height*width)
       
        if mode=="original":
            for i in range(pred.shape[0]):
                for pix in range(pred.shape[1]):
                    pred_images[i,pix]=pred[i,pix,1]
            

        elif mode=="threshold":
            for i in range(pred.shape[0]):
                for pix in range(pred.shape[1]):

                    if pred[i,pix,1]>=thrhold:
                        pred_images[i,pix]=1 
                    else:
                        pred_images[i,pix]=0
   
        else:
            print("mode " +str(mode) +" not recognized, it can be original or threshold")
            exit()
   
        pred_images = np.reshape(pred_images,(pred_images.shape[0],1, patch_height, patch_width))
        return pred_images.astype(np.float)



    def load_np_trainset(self, path_data, path_masks, path_base_data):

        print('Loading masks data... ')
        del self.masks
        self.masks, self.masks_n = self.load_np_data(path_masks,self.do_normalize_masks)# self.normalize_0_1(self.load_np_data(path_masks))
        
        if (self.check_mask(self.masks) == 0):
            print('Warning, checkup mask or control mask are not 0-1:') 
        
        if (self.diff_method=='-nd'): 
            print('Loading train data... ')        
            del self.data
            self.data, self.data_frames_n = self.load_np_data(path_data,self.do_normalize_data )  
       
        elif (self.diff_method=='-bd'): 
            
            print('Loading train data... ') 
            self.data, self.data_frames_n = self.load_np_data(path_data,self.do_normalize_data )  
       
            print('Loading base_diff data...') 
            self.base_data,self.base_files_n = self.load_np_data(path_base_data, self.do_normalize_data) 


            frame_n = 0 
            for frame_i in range(1,self.base_data_frames_n):
                
                frame_n=frame_n+1
                
                curr_base_data = np.copy(self.base_data[file_i,0,:,:])
                if (frame_i==1): 
                    sum_base_data=np.copy(curr_base_data)
                else: 
                    sum_base_data=np.add(sum_base_data, curr_base_data) 
            
            self.base_diff= np.divide(sum_base_data, frame_n)


            for frame_i in range(1,self.data_frames_n):

                curr_data = np.copy(self.data[file_i,0,:,:])
                data_sub = np.subtract(curr_data,self.base_diff) 
                curr_data_bd=np.divide(data_sub,curr_data) 
                np.copy(self.data[file_i,0,:,:], curr_data_bd)
                                           

        elif (self.diff_method=='-rd'): 
            
            print('Loading train data... ') 
            self.data, self.data_frames_n = self.load_np_data(path_data,self.do_normalize_data )  
       
            for frame_i in range(1,self.data_frames_n):
                
                curr_data = np.copy(self.data[frame_i,:,:,:])  
       
                if (frame_i==1):
                    curr_zero = np.subtract(self.data[frame_i,:,:,:],self.data[frame_i,:,:,:])
                    self.data[frame_i,:,:,:] = np.copy(curr_zero)

                else:
                    curr_data_sub = np.subtract(curr_data,prev_curr_data)
                    
                    if (self.norm_rd_by_itself==True):
                           curr_data_div = np.divide(curr_data_sub,curr_data)
                           self.data[frame_i,:,:,:] = np.copy(curr_data_div)
                    else:
                           self.data[frame_i,:,:,:] = self.normalize_0_1(curr_data_sub)  

                prev_curr_data=np.copy(curr_data)

        else: 
            print('Wrong image differencing flag: '+self.diff_method);
            print('Supposed to be -nd(no difference), -rd(running difference) or -bd(base difference');
            sys.exit()
        
        if (self.data_frames_n!=self.masks_n):
            print('Error: amount of data and mask files is not equal! Corrupt training set: '+ str(self.data_frames_n)+',' + str(self.masks_n))
            sys.exit()


    def data_2_trainset(self):

        print('Preparing training set...')
        pred = self.masks_Unet(self.masks)
        rest_masks=self.pred_to_imgs(pred, self.size_x, self.size_y, mode="threshold")
          
        this_masks_Unet=self.masks_Unet(rest_masks)

              
        self.train_dataset = tf.data.Dataset.from_tensor_slices((self.data, this_masks_Unet))
 
        if (self.do_shuffle==True):
            print('Shuffle...')
            self.train_dataset = self.train_dataset.shuffle(self.shuffle_buffer_size).batch(self.batch_size)
        
        #HOW DOES SHUFFLE WORKS, RANDOM MANUALLY ? 

        print('dataset.batch...')
        self.train_dataset = self.train_dataset.batch(self.batch_size)

        
    def load_np_data_this(self, path_data):        
        
        print('Loading data... ')
        self.data=self.load_np_data(path_data)  
  


    '''
    def calc_patches(self) 
        #train_data_preproc = my_PreProc(data)
        #train_masks_preproc = masks/255 
        self.patches_train_data, self.patches_train_masks = extract_random(self.data_train,self.masks,self.patch_height,self.patch_width,self.n_subimgs,self.inside_FOV)
       

    def train_model_pathces(self):
        
        #data_train = my_PreProc(data)
        #masks_train = masks/255 achtung: check if they are not binary already
        # data_train = self.data
        # masks_train = self.masks
        self.patches_imgs_train, self.patches_masks_train = extract_random(self.imgs_train,self.masks.train,self.patch_height,self.patch_width,self.n_subimgs,self.inside_FOV)
        n_sample = min(patches_imgs_train.shape[0],40)
        self.model = get_unet(self.n_channels, self.patch_height, self.patch_width)
        patches_masks_train = self.masks_Unet(patches_masks_train)  #reduce memory consumption 
        #checkpointer = ModelCheckpoint(filepath='./'+name_experiment+'/'+name_experiment +'_best_weights.h5', verbose=1, monitor='val_loss', mode='auto', save_best_only=True) #save at each epoch if the validation decreased
        self.model.fit(patches_imgs_train, patches_masks_train, nb_epoch=self.n_epochs, batch_size=self.batch_size, verbose=2, shuffle=True, validation_split=0.1, callbacks=[checkpointer])
    '''    


    def  model_train_straight(self):
          
        #check if  '_'+model_id +'.json' files exist...
        print('Training model. Epochs: '+str(self.epochs_n)+' Batch_size:'+ str(self.batch_size))       
        #self.model.fit(self.data, self.masks, epochs=self.epochs_n) #, batch_size=self.batch_size, verbose=2, shuffle=self.shuffle, validation_split=self.validation_split)
     
        self.model.fit(self.train_dataset, epochs=self.epochs_n)            
     
    
    def model_save(self):
        print('Saving the model')
        json_string = self.model.to_json()
        open(self.shape_name +'_'+str(self.model_id) +'.json', 'w').write(json_string)
        self.model.save_weights(self.model_name + '_'+str(self.model_id)+'.h5', overwrite=True)

  
    def model_save_interim(self, angle, sigma): 
        print('Saving interim model for engineered features with angle & sigma: '+ str(angle)+','+str(sigma))
        json_string = self.model.to_json()
        open(self.interim_model_path+self.shape_name +'_'+str(self.model_id)+'_' +str(angle)+'_'+str(sigma)+'.json', 'w').write(json_string)
        self.model.save_weights(self.interim_model_path+self.model_name + '_'+str(self.model_id)+'_' +str(angle)+'_'+str(sigma)+'.h5', overwrite=True)


    def model_load(self,model_id):
          
        try:
            self.model = tf.keras.models.model_from_json(open(self.shape_name +'_'+str(model_id) +'.json').read())
            self.model.load_weights(self.model_name + '_'+str(model_id)+'.h5') 
            self.model_id = model_id 
            self.model.compile(optimizer='sgd', loss='categorical_crossentropy',metrics=['accuracy'])
            return model_id
        except: 
            return 0
            #sys.exit()


    def model_load_interim(self,model_id,angle,sigma):
          
        try:
            self.model = tf.keras.models.model_from_json(open(self.interim_model_path+self.shape_name +'_'+str(model_id)+'_' +str(angle)+'_'+str(sigma)+'.json').read())
            self.model.load_weights(self.interim_model_path+self.model_name + '_'+str(model_id)+'_' +str(angle)+'_'+str(sigma)+'.h5') 
            self.model_id = model_id 
            self.model.compile(optimizer='sgd', loss='categorical_crossentropy',metrics=['accuracy'])
            return model_id
        except: 
            return 0
            #sys.exit()
 

    def model_evaluate(self):
       
        print('Model evaluation...')
        loss,acc = self.model.evaluate(self.train_dataset, verbose=2 )
        print("Model, accuracy: {:5.2f}%".format(100 * acc))
        return loss,acc        


    def check_mask(self, mask):
        
        if ( (np.amax(mask)>1) or (np.amin(mask)<0) ): 
           return 0 
        else: 
           return 1 


    def masks_compare(self, test_mask, ctl_mask): 
        

        if  ( (self.check_mask(test_mask)==0) or (self.check_mask(ctl_mask)==0) ): 

            print('Warning, checkup mask or control mask are not 0-1:') 
            
            print('Test mask max:')
            np.amax(test_mask)
            print('Test mask min:')
            np.amin(test_mask)

            print('Control mask max:')
            np.amax(ctl_mask)
            print('Control mask min:')
            np.amin(ctl_mask)

        test_mask_norm= self.normalize_0_1(test_mask) 
       
        #test_mask_d = np.ravel(test_mask) 
        #ctl_mask_d = np.ravel(ctl_mask)
        test_mask_nz = (test_mask_norm>self.test_mask_nonzero_coeff)
        ctl_mask_nz = (ctl_mask>self.ctl_mask_nonzero_coeff)

        test_mask_nz_count= np.count_nonzero(test_mask_nz)
        ctl_mask_nz_count = np.count_nonzero(ctl_mask_nz)
        
        print('Test mask pixels count: '+ str(test_mask_nz_count)+ ' Control mask pixels count:'+str(ctl_mask_nz_count))

        correspond_mask=np.logical_and(test_mask_nz,ctl_mask_nz) 
        #correspond_mask_add=np.add(test_mask_nz,ctl_mask_nz)
        #correspond_mask_nz = (correspond_mask_add>test_mask_nz)

        correspond_mask_nz_count = np.count_nonzero(correspond_mask)
        
        
        if (test_mask_nz_count > 0):
            return correspond_mask_nz_count/test_mask_nz_count
        
        else: 
            print('Masks_compare warning, empty test mask!')
            print('Check mask non zero pixels:'+ str(test_mask_nz_count))
            print('Ctl mask non zero pixels:'+str(ctl_mask_nz_count))
            print('np.logical_and(test_mask,ctl_mask):'+str(correspond_mask_nz_count))
            return 0 




    def model_predict(self):

        print('Running predictions:')
        predictions_1d = self.model.predict(self.data, verbose=self.verbose )
        masks_1d = self.masks_Unet(self.masks)
        self.predictions= self.pred_to_imgs(predictions_1d, self.size_x, self.size_y,"original") #"original")
        self.predictions[np.isnan(self.predictions)] = 0
    

        print("Control. Predicted images size :")
        print(self.predictions.shape)
        print("Control. Masks images size :")
        print(self.masks.shape)
        print("Control. Data images size:")
        print(self.data.shape)
        print("Predictions Max pixel value")
        print(np.amax(self.predictions))
        print("Predictions Min pixel value")
        print(np.amin(self.predictions)) 

        corr_ind = 0 
        frame_n = 0 

        corr_ind_min = 100000
        corr_ind_max = 0
    
        for frame_i in range(1,self.masks_n):       
            frame_n=frame_n+1
            #corr_ind=corr_ind+self.masks_compare(self.predictions[frame_i,0], self.masks[frame_i,0])            
            
            #corr_ind_this=self.masks_compare(predictions_1d[frame_i], masks_1d[frame_i]) 
            corr_ind_this=self.masks_compare(np.copy(self.predictions[frame_i]), self.masks[frame_i]) 
            
            if (frame_n>0): 
                print('Correspondence between predicted and control mask: ' + str(corr_ind_this)+ ' for a sample no: '+str(frame_n))             

            corr_ind=corr_ind+corr_ind_this      
          
            if (corr_ind_max<corr_ind_this):
                corr_ind_max=corr_ind_this

            if (corr_ind_min>corr_ind_this):
                corr_ind_min=corr_ind_this



        corr_ind = corr_ind/frame_n 

        print('Average correspondene between control and predicted datasets: ' + str(corr_ind))
        print('       for: '+str(frame_n)+' test samples')
        print('       Max: '+str(corr_ind_max)+' Min:' +str(corr_ind_min))        
        return corr_ind


    def model_predict_obj(self, model_obj):

        print('Running predictions:')
        predictions_reduced_dim = model_obj.predict(model_obj.data, verbose=self.verbose )
        model_obj.predictions= model_obj.pred_to_imgs(predictions_reduced_dim, model_obj.size_x, model_obj.size_y,"threshold") #"original")
        #self.predictions = np.transpose(self.predictions,(0,3,1,2))
        #print("Control. Predicted images size :")
        #print(model_obj.predictions.shape)
        return model_obj.predictions.shape

    
    def compare_predictions_w_ctl(self, model_ctl):    
        
        corr_ind = 0 
        frame_n = 0 
        #print('Debug:')
        for frame_i in range(1,model_ctl.data_frames_n):       
            
            frame_n=frame_n+1
            corr_ind=corr_ind+self.masks_compare(self.predictions[frame_i], model_ctl.masks[frame_i])            
            
            #print('frame_n')
            #print('corr_ind')
            #print(frame_n)
            #print(corr_ind)

        corr_ind = corr_ind/frame_n 

        #print('Average correspondene between control and predicted datasets:' + corr_ind)
        return corr_ind
        
       
    def compare_predictions_2_obj(self, ml_preds, ml_ctl): 

        corr_ind = 0 
        frame_n = 0 
        for frame_i in range(1,ml_preds.data_frames_n):       
            corr_ind=corr_ind+ml_preds.masks_compare(ml_preds.predictions[frame_i], ml_ctl.masks[frame_i])            
    
        corr_ind = corr_ind/frame_n 

        #print('Average correspondene between control and predicted datasets:' + corr_ind)
        return corr_ind


    def save_predictions(self):
        
        print('Saving predictions...')
        
        pred = self.masks_Unet(self.masks)
        rest_masks=self.pred_to_imgs(pred, self.size_x, self.size_y, mode="threshold")

        for frame_i in range(1,self.data_frames_n):
        
            fname_predict_np = self.predict_path +'ml_mask_'+str(self.model_id) +'_'+str(frame_i)  
            fname_data = self.predict_path+'data_'+str(self.model_id)+'_'+str(frame_i)
            fname_predict_img = self.predict_path +'ml_mask_'+str(self.model_id) +'_'+str(frame_i)+'.png'     
            fname_data_img = self.predict_path+'data_'+str(self.model_id)+'_'+str(frame_i)+'.png'
            fname_data_2_img = self.predict_path+'data_2_'+str(self.model_id)+'_'+str(frame_i)+'.png'
            fname_data_3_img = self.predict_path+'data_3_'+str(self.model_id)+'_'+str(frame_i)+'.png'
            

            fname_mask_img_ctl = self.predict_path+'mask_ctl_'+str(self.model_id)+'_'+str(frame_i)+'.png'
        
            #data_unet=self.masks_Unet(self.data[frame_i]) 
            #data_un_unet=self.pred_to_imgs(data_unet, self.size_x, self.size_y, mode="original")
           
            print(fname_data)
            print(fname_predict_np) 
            #saving numpy data
            print('Saving initial and predicted data (numpy arrays)')
            np.save(fname_predict_np, self.predictions[frame_i], allow_pickle='false', fix_imports='false')
            np.save(fname_data, self.data[frame_i], allow_pickle='false', fix_imports='false' )          
            #np.save(fname_data, data_un_unet, allow_pickle='false', fix_imports='false' )          
            
            #HERE. see /ver_1/take_4...

            #saving images
            print('Saving initial and predicted data (png)')
                     
            save_pred_struct = t_struct_eng()
            save_pred_struct.data = np.copy(self.predictions[frame_i,0])
            save_pred_struct.save_data_png_ri(fname_predict_img);   

            save_masks_struct = t_struct_eng()
            save_masks_struct.data = np.copy(rest_masks[frame_i,0])
            save_masks_struct.save_data_png_ri(fname_mask_img_ctl);            

            save_data_struct = t_struct_eng()
            save_data_struct.data = np.copy(self.data[frame_i,0]) 
            save_data_struct.save_data_png_ri(fname_data_img);

            save_data_struct = t_struct_eng()
            save_data_struct.data = np.copy(self.data[frame_i,0]) 
            save_data_struct.save_data_png(fname_data_2_img);

            plt.imsave(arr = self.data[frame_i,0], fname= fname_data_3_img, cmap='gray')
        

      

            #debug:
            #for x in range(0,self.size_x):
            #    for y in range(0,self.size_y):
            #        if (self.masks[frame_i,0,x,y]!=0):  
                    #if (True):         
            #            print('x: '+str(x)+'y:'+str(y)+'val:' +str(self.masks[frame_i,0,x,y]))



    def save_control(self,angle, dyn_sigma):
        
        print('Saving control data for engenereed features...')
        
        pred = self.masks_Unet(self.masks)
        rest_masks=self.pred_to_imgs(pred, self.size_x, self.size_y, mode="threshold")

        for frame_i in range(1,self.masks_n-1):
        
            fname_data_img_ri = self.control_path+'data_ri_'+str(self.model_id)+'_'+str(frame_i)+'_ang_'+str(angle)+'_dyn_'+str(dyn_sigma)+'.png'
            fname_mask_img_ri = self.control_path+'mask_ri_'+str(self.model_id)+'_'+str(frame_i)+'_ang_'+str(angle)+'_dyn_'+str(dyn_sigma)+'.png'
            
            fname_data_img = self.control_path+'data_'+str(self.model_id)+'_'+str(frame_i)+'_ang_'+str(angle)+'_dyn_'+str(dyn_sigma)+'.png'
            fname_mask_img = self.control_path+'mask_'+str(self.model_id)+'_'+str(frame_i)+'_ang_'+str(angle)+'_dyn_'+str(dyn_sigma)+'.png'
            
            fname_data = self.control_path+'data_'+str(self.model_id)+'_'+str(frame_i)+'_ang_'+str(angle)+'_dyn_'+str(dyn_sigma)
            fname_mask = self.control_path+'mask_'+str(self.model_id)+'_'+str(frame_i)+'_ang_'+str(angle)+'_dyn_'+str(dyn_sigma)
    
            #saving numpy data
            print('Saving enginered data (numpy arrays)')
            np.save(fname_data, self.data[frame_i], allow_pickle='false', fix_imports='false')
            np.save(fname_mask, self.masks[frame_i], allow_pickle='false', fix_imports='false' )          
            #np.save(fname_data, data_un_unet, allow_pickle='false', fix_imports='false' )          
            
            #saving images
            print('Saving initial and predicted data (numpy arrays)')
                     
            save_data_struct_ri = t_struct_eng()
            save_data_struct_ri.data = np.copy(self.data[frame_i,0])
            save_data_struct_ri.save_data_png_ri(fname_data_img_ri);   

            save_data_struct = t_struct_eng()
            save_data_struct.data = np.copy(self.data[frame_i,0])
            save_data_struct.save_data_png(fname_data_img);   
           
            del save_data_struct_ri
            del save_data_struct
           
                      
            save_mask_struct_ri = t_struct_eng()
            save_mask_struct_ri.data = np.copy(self.masks[frame_i,0])
            save_mask_struct_ri.save_data_png_ri(fname_mask_img_ri);   

            save_mask_struct = t_struct_eng()
            save_mask_struct.data = np.copy(self.masks[frame_i,0])
            save_mask_struct.save_data_png(fname_mask_img); 
            
            del save_mask_struct_ri
            del save_mask_struct




    def __init__(self, files_n, max_files_n):
        
        
        self.files_n=files_n 
        self.max_files_n = max_files_n
        self.data_frames_n = min(files_n,max_files_n) 
        self.mask_n = min(files_n,max_files_n) 

        self.base_max_files_n = 8; 
        self.base_files_n = 0; 
     

        self.model_id = 0
        self.channels_n = 1 # Grayscale = 1, RGB = 3
        self.size_x = 264
        self.size_y = 264

        self.data=np.empty([self.data_frames_n,self.channels_n,self.size_x,self.size_y],dtype=float)
        self.base_data = np.empty([self.base_max_files_n,self.channels_n,self.size_x,self.size_y],dtype=float)
        self.masks=np.empty([self.data_frames_n,self.channels_n,self.size_x,self.size_y],dtype=float)
        self.predictions=np.empty([self.data_frames_n,self.channels_n,self.size_x,self.size_y],dtype=float)
        
        self.train_dataset=[] 
        
        self.diff_method = '-nd' # 'bd' 'rd'
        self.norm_rd_by_itself=False
        
        self.do_normalize_data = True 
        self.do_normalize_masks = True  

        #Model training parameters
        self.epochs_n = 12
        self.batch_size = 4 #none
        self.do_shuffle = False
        self.shuffle_buffer_size = 100
        #self.validation_split = 0.1
        self.verbose = 2

        #Patches configuaration 
        self.patch_height = 200
        self.patch_width = 200
        self.n_subimgs = 40
        self.inside_FOV = 1

        #Masks comparizon coeffs. 
        self.test_mask_nonzero_coeff = 0.5 #for non binary masks normalized to [0,1] interval
        self.ctl_mask_nonzero_coeff = 0  

        self.shape_name = 'model_shape'
        self.model_name = 'model_weights'
        self.predict_path = './predictions/'
        self.control_path = './control/'
        self.base_data_path = './base_data/'
        self.interim_model_path = './interim_models_log/'
        self.verify_log_self_fname = '/verify_log_self'
        self.verify_log_testdata_fname = '/verify_log_testdata'