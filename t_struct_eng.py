import numpy as np
import matplotlib.pyplot as plt
from skimage import io, exposure, img_as_uint, img_as_float
from scipy import misc
from scipy.ndimage import rotate
import cv2
import pyfits

class t_struct_eng(object):

    # Feature enginering routines 

    
    def rotate(self, vector, theta, rotation_around=None) -> np.ndarray:
        """
         reference: #In_two_dimensions
        :param vector: list of length 2 OR
                      list of list where inner list has size 2 OR
                      1D numpy array of length 2 OR
                      2D numpy array of size (number of points, 2)
        :param theta: rotation angle in degree (+ve value of anti-clockwise rotation)
        :param rotation_around: "vector" will be rotated around this point, 
                      otherwise [0, 0] will be considered as rotation axis
        :return: rotated "vector" about "theta" degree around rotation
                axis "rotation_around" numpy array
        """
        vector = np.array(vector)

        if vector.ndim == 1:
           vector = vector[np.newaxis, :]

        if rotation_around is not None:
           vector = vector - rotation_around

        vector = vector.T

        theta = np.radians(theta)

        rotation_matrix = np.array([
            [np.cos(theta), -np.sin(theta)],
            [np.sin(theta), np.cos(theta)]
        ])

        output: np.ndarray = (rotation_matrix @ vector).T

        if rotation_around is not None:
            output = output + rotation_around

        return output.squeeze() 



    def load_np_data(self, fname):
        self.data=np.load(fname) 
        print(self.data.shape)
        print(np.amax(self.data))

    def load_fits_data(self, fname):
        full_data = pyfits.open(fname)
        full_data.info()
        # Load table data as tbdata 
        self.data = full_data[0].data
        print(self.data.shape)
        print(np.amax(self.data))    


    def rotate_data(self, theta, rotation_around=None):
        self.rotate(self.data, theta, rotation_around) 
        


    def rotate_data_scipy(self, theta, replace_bkg):
   
        data_rt = rotate(self.data,theta) 
        
        data_shape = np.shape(self.data) 
        data_rt_shape = np.shape(data_rt)
       
        x_1 = round(data_rt_shape[0]/2-data_shape[0]/2)      
        x_n = round(data_rt_shape[0]/2+data_shape[0]/2)   
        
        y_1 = round(data_rt_shape[1]/2-data_shape[1]/2)      
        y_n = round(data_rt_shape[1]/2+data_shape[1]/2)   

        data_rt_crop=data_rt[x_1:x_n, y_1:y_n]
        
        if (replace_bkg=='true'):
            zero_mask = np.multiply( (data_rt_crop == 0), self.data)
            data_rt_crop_bend= np.add( data_rt_crop, zero_mask )
            self.data = data_rt_crop_bend
        
        else: 
            self.data = data_rt_crop
            

        #return data_rt_crop



    def window_filt_ext(self, data,  boxsize, increment, thrhold, size_x, size_y): 
           
        result_data = np.copy(data)
           
           
        for x_1 in range(1,size_x-boxsize, increment):
            for y_1 in range(1,size_y-boxsize, increment): 
               
                x_m=x_1+boxsize
                y_m=y_1+boxsize 

                border_horz_l=data[x_1:x_m, y_1:(y_1+1)]
                border_horz_r=data[x_1:x_m, y_m:(y_m+1)]
                
            
                border_vert_l=data[x_1:(x_1+1), y_1:y_m]
                border_vert_r=data[x_m:(x_m+1), y_1:y_m]

                border_horz_l_max=np.amax(border_horz_l)
                border_horz_r_max=np.amax(border_horz_r)
           
                border_vert_l_max=np.amax(border_vert_l)
                border_vert_r_max=np.amax(border_vert_r)
               
                border_horz_l_min=np.amin(border_horz_l)
                border_horz_r_min=np.amin(border_horz_r)
           
                border_vert_l_min=np.amin(border_vert_l)
                border_vert_r_min=np.amin(border_vert_r)

                area_mean = np.mean(result_data[x_1+1:x_m-1, y_1+1:y_m-1])
                  
                borders_mean =  (np.mean(border_horz_l)+np.mean(border_horz_r)+
                              np.mean(border_vert_l)+np.mean(border_horz_r)) /4

                '''
                if (borders_mean>0): 
                    print('Debug borders >0')

                if (area_mean>0):
                    print('Area mean > 0')
                
                '''
                if ((border_horz_l_max<=thrhold ) and (border_horz_r_max<=thrhold) and (border_vert_l_max<=thrhold) and (border_vert_r_max<=thrhold)):              
                 
                    '''
                    for x_i in range(x_1,x_m, 1):
                        for y_i in range(y_1,y_m,1):
                            result_data[x_i, y_i]=0
            
                    '''
                    result_data[x_1:x_m, y_1:y_m] = 0 #np.zeros( (boxsize,boxsize), dtype = type(data))
                    
                

                               
                elif ((border_horz_l_min>thrhold ) and (border_horz_r_min>thrhold) and (border_vert_l_min>thrhold) and (border_vert_r_min>thrhold)):              
                    
                   #  replace for loop with multiplyin matrixes

                    for x_i in range(x_1+1,x_m-1, 1):
                       for y_i in range(y_1+1,y_m-1,1):
                           if (result_data[x_i,y_i]<=thrhold):
                               result_data[x_i,y_i]=borders_mean#area_mean
                
         
            
        return result_data
        


                  
    def window_filt(self, boxsize, increment, thrhold):     

        size_x= np.size(self.data,0)
        size_y= np.size(self.data,1)

        self.data=np.copy(self.window_filt_ext(self.data,  boxsize, increment, thrhold, size_x, size_y))

    
    


    def fits_2_png16_single_cv2(self, input_fname, output_fname, size_x, size_y):
    
        print(input_fname)
        full_data = pyfits.open(input_fname)
        full_data.info()
        # Load table data as tbdata 
        img_arr = full_data[0].data
        #im = np.array([[1., 2.], [3., 4.]], dtype='float64') 
        img_arr = exposure.rescale_intensity(img_arr, out_range='float')
        img_arr = img_as_uint(img_arr)
        img_arr = cv2.resize(img_arr, dsize=(size_x, size_y), interpolation=cv2.INTER_CUBIC) 
        cv2.imwrite(output_fname, img_arr, [cv2.IMWRITE_PNG_COMPRESSION, 0]) #  No compression 

    

    def save_data_png_ri(self, fname ):
    
        img_arr = exposure.rescale_intensity(self.data, out_range='float')
        img_arr = img_as_uint(img_arr)
        cv2.imwrite(fname, img_arr, [cv2.IMWRITE_PNG_COMPRESSION, 0]) #  No compression 




    #def save_data_png(fname, size_x, size_y):
    def save_data_png(self, fname):
    
        #img_arr = exposure.rescale_intensity(self.data, out_range='float')
        #img_arr = img_as_uint(img_arr)
        #img_arr = cv2.resize(img_arr, dsize=(size_x, size_y), interpolation=cv2.INTER_CUBIC) 
        #cv2.imwrite(fname, img_arr, [cv2.IMWRITE_PNG_COMPRESSION, 0]) #  No compression 
        cv2.imwrite(fname, self.data, [cv2.IMWRITE_PNG_COMPRESSION, 0]) #  No compression 


    def save_data_np(self, fname):

        np.save(fname, self.data, allow_pickle='false', fix_imports='false')


  

  