#!/bin/bash

echo "Wavetrack: Trainset masks filter tool based oun the Wavetrack output. Oleg Stepanyuk, 2022"

echo "Enter input masks path (numpy): "
read input_path

echo "Enter input masks path (png):"
read input_path_png

echo "Input window filter size:"
read boxsize 
echo "Input increment:"
read increment

cp -rf $input_path $input_path.save

for fname in "$input_path"/*
do 

    echo "Processing:" $fname
    
    fname_png=${fname}.png 
    echo $fname_png
    python3 sscale_filt_2.py $fname $fname $fname_png $boxsize $increment

done

mkdir $input_path_png
mv $input_path/*.png $input_path_png

