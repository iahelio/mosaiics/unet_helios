from t_struct_eng import t_struct_eng
import sys

struct_eng = t_struct_eng()


print('Small-scale noize filter from Wavetrack package. By Oleg Stepanyuk 2021')


if ((len(sys.argv)-1) < 5):
    print('Syntax: python3 sscale_filt.py <input_file.fits/npy> <output_file.npy> <output_file.png> <boxsize> <increment> ')

else: 

    #here why sys not found
    #if ( (sys.agrv[1].find('fits')>0) or (sys.agrv[1].find('fts')>0) ):
    #    print('Loading fits file:'+sys.argv[1])
    #    struct_eng.load_fits_data(sys.argv[1])
    
    #else:
    #    print('Loading numpy dat file:'+sys.argv[1])   
    #    struct_eng.load_np_data(sys.argv[1])
    struct_eng.load_np_data(sys.argv[1])
    

    struct_eng.window_filt(int(sys.argv[4]),int(sys.argv[5]),0) 

    struct_eng.save_data_np(sys.argv[2])
    struct_eng.save_data_png_ri(sys.argv[3])
    struct_eng.save_data_png(sys.argv[3])
   