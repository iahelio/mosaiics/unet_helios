#!/bin/bash
echo "Enter timestep NNN: "
read timestep_num
echo "Enter timestep NN:"
read timestep_atrous_num

mkdir ./chosen_timestep_$timestep_num
mkdir ./chosen_timestep_atrous_$timestep_atrous_num
 
cp ./visualize/masks_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./visualize/masks_grads_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./visualize/masks_raw_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./visualize/masks_raw_w_grads_projected_bkg_2_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./visualize/masks_grads_projected_BASE_DIFF_data_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./vizualize/masks_projected_BASE_DIFF_data_bw_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./visualize/masks_raw_rnb_projected_bkg_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./visualize/bkg_bw_t_$timestep_num* ./chosen_timestep_$timestep_num/

cp ./trainset/masks_applied_BASE_DIFF_data_t_$timestep_num* ./chosen_timestep_$timestep_num/
cp ./trainset/BASE_DIFF_data_t_$timestep_num* ./chosen_timestep_$timestep_num/

cp ./output_log/out_t_$timestep_atrous_num* ./chosen_timestep_atrous_$timestep_atrous_num/





