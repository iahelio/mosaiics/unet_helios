#!/bin/bash

echo "Wavetrack: Trainset preparation script based oun the Wavetrack output. Oleg Stepanyuk, 2021"

echo "Input event code:"
read wavetrack_code
echo "Enter wavetrack output path"
read input_path
echo "Enter first timestep: "
read timestep_num_start
echo "Enter last timestep:"
read timestep_num_end 
echo "Enter timestep interval:"
read timestep_interval
echo "Enter output_path for trainset (randomized):"
read output_path_rand

# Local paths of wavetrack output and tensorflow trainsets 

tf_raw_input_loc_path_rand_full="input_vec_raw_rand/"
tf_diff_input_loc_path_rand_full="input_vec_diff_rand/"

tf_raw_output_loc_path_rand_full="output_vec_raw_rand/"
tf_diff_output_loc_path_rand_full="output_vec_diff_rand/"
tf_masks_output_loc_path_rand_full="output_vec_masks_rand/"

tf_raw_input_loc_path_rand_full_png16="input_vec_raw_rand_png16/"
tf_diff_input_loc_path_rand_full_png16="input_vec_diff_rand_png16/"
tf_masks_input_loc_path_rand_full_png16="input_vec_masks_rand_png16/"

tf_raw_output_loc_path_rand_full_png16="output_vec_raw_rand_png16/"
tf_diff_output_loc_path_rand_full_png16="output_vec_diff_rand_png16/"
tf_masks_output_loc_path_rand_full_png16="output_vec_masks_rand_png16/"

tf_raw_input_loc_path_rand_full_numpy="input_vec_raw_rand_numpy/"
tf_diff_input_loc_path_rand_full_numpy="input_vec_diff_rand_numpy/"
tf_masks_input_loc_path_rand_full_numpy="input_vec_masks_rand_numpy/"

tf_raw_output_loc_path_rand_full_numpy="output_vec_raw_rand_numpy/"
tf_diff_output_loc_path_rand_full_numpy="output_vec_diff_rand_numpy/"
tf_masks_output_loc_path_rand_full_numpy="output_vec_masks_rand_numpy/"


# Tensorflow vector global path with random coefficients 

tf_raw_input_glob_path_rand_full="$output_path_rand$tf_raw_input_loc_path_rand_full"
tf_diff_input_glob_path_rand_full="$output_path_rand$tf_diff_input_loc_path_rand_full"

tf_raw_output_glob_path_rand_full="$output_path_rand$tf_raw_output_loc_path_rand_full"
tf_diff_output_glob_path_rand_full="$output_path_rand$tf_diff_output_loc_path_rand_full"
tf_masks_output_glob_path_rand_full="$output_path_rand$tf_masks_output_loc_path_rand_full"


# Tensorflow input vector global paths (random) (png16)
tf_raw_input_glob_path_rand_full_png16="$output_path_rand$tf_raw_input_loc_path_rand_full_png16"
tf_diff_input_glob_path_rand_full_png16="$output_path_rand$tf_diff_input_loc_path_rand_full_png16"  
tf_masks_input_glob_path_rand_full_png16="$output_path_rand$tf_masks_input_loc_path_rand_full_png16"

# Tensorflow output vector global paths (random) (png16)
tf_raw_output_glob_path_rand_full_png16="$output_path_rand$tf_raw_output_loc_path_rand_full_png16"
tf_diff_output_glob_path_rand_full_png16="$output_path_rand$tf_diff_output_loc_path_rand_full_png16"  
tf_masks_output_glob_path_rand_full_png16="$output_path_rand$tf_masks_output_loc_path_rand_full_png16"


# Tensorflow input vector global paths (random) (numpy)
tf_raw_input_glob_path_rand_full_numpy="$output_path_rand$tf_raw_input_loc_path_rand_full_numpy"
tf_diff_input_glob_path_rand_full_numpy="$output_path_rand$tf_diff_input_loc_path_rand_full_numpy"  
tf_masks_input_glob_path_rand_full_numpy="$output_path_rand$tf_masks_input_loc_path_rand_full_numpy"


# Tensorflow output vector global paths (random) (numpy)
tf_raw_output_glob_path_rand_full_numpy="$output_path_rand$tf_raw_output_loc_path_rand_full_numpy"
tf_diff_output_glob_path_rand_full_numpy="$output_path_rand$tf_diff_output_loc_path_rand_full_numpy"  
tf_masks_output_glob_path_rand_full_numpy="$output_path_rand$tf_masks_output_loc_path_rand_full_numpy"



mkdir $tf_raw_input_glob_path_rand_full
mkdir $tf_diff_input_glob_path_rand_full
mkdir $tf_raw_output_glob_path_rand_full
mkdir $tf_diff_output_glob_path_rand_full
mkdir $tf_masks_output_glob_path_rand_full

mkdir $tf_raw_input_glob_path_rand_full_png16
mkdir $tf_diff_input_glob_path_rand_full_png16
mkdir $tf_raw_output_glob_path_rand_full_png16
mkdir $tf_diff_output_glob_path_rand_full_png16
mkdir $tf_masks_output_glob_path_rand_full_png16

mkdir $tf_raw_input_glob_path_rand_full_numpy
mkdir $tf_diff_input_glob_path_rand_full_numpy
mkdir $tf_raw_output_glob_path_rand_full_numpy
mkdir $tf_diff_output_glob_path_rand_full_numpy
mkdir $tf_masks_output_glob_path_rand_full_numpy

timestep_num=$timestep_num_start
pipeline_timestep=$pipeline_timestep_str

while [ $timestep_num -lt $timestep_num_end ] 
do 

    echo "Processing" $timestep_num "timestep"

    rand_timestep_num=$RANDOM

    timestep_str_3d=$(printf '%03d' $timestep_num) 
    pipeline_timestep_str_6d=$(printf '%06d' $pipeline_timestep)  
    rand_timestep_str_8d=$(printf '%08d' $rand_timestep_num)

    #random indexes trainsets     
    # Typical output 
    # orig_data_t_168-?_????-??-?????:??:??.???.png
    # BASE_DIFF_data_t_174-?_????-??-?????:??:??.???.fits
    # constructing base-difference based traiset (RANDOM indexes)

    # RANDOM + Initial Indexes
    
    # constructing base-difference based traiset (RANDOM indexes + Initial Indexes)
    cp ${input_path}trainset/BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_diff_input_glob_path_rand_full/BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.fits
    cp ${input_path}trainset/masks_applied_BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_diff_output_glob_path_rand_full/masks_applied_BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.fits 
    cp ${input_path}trainset/BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_diff_input_glob_path_rand_full/BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png
    cp ${input_path}trainset/masks_applied_BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_diff_output_glob_path_rand_full/masks_applied_BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png 

    #constructing RAW data based trainset (RANDOM indexes+ Initial Indexes)
    cp ${input_path}trainset/orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_raw_input_glob_path_rand_full/orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.fits
    cp ${input_path}trainset/masks_applied_orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits $tf_raw_output_glob_path_rand_full/masks_applied_orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.fits
    cp ${input_path}trainset/orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_raw_input_glob_path_rand_full/orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png
    cp ${input_path}trainset/masks_applied_orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.png $tf_raw_output_glob_path_rand_full/masks_applied_orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png

    #constructing masks - based trainset (RANDOM indexes + Initial Indexes)
    cp ${input_path}visualize/masks_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits  $tf_masks_output_glob_path_rand_full/masks_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.fits
    cp ${input_path}visualize/masks_t_$timestep_str_3d-?_????-??-?????:??:??.???.png  $tf_masks_output_glob_path_rand_full/masks_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png

    # PNG16 NPY
    # constructing base-difference based traiset (RANDOM indexes + Initial Indexes)

    python3 fits_2_png16.py ${input_path}trainset/BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits temp.png temp 264 264 
    mv ./temp.png $tf_diff_input_glob_path_rand_full_png16/BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png
    mv ./temp.npy $tf_diff_input_glob_path_rand_full_numpy/BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.npy

    python3 fits_2_png16.py ${input_path}trainset/masks_applied_BASE_DIFF_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits temp2.png temp2 264 264
    mv ./temp2.png $tf_diff_output_glob_path_rand_full_png16/masks_applied_BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png 
    mv ./temp2.npy $tf_diff_output_glob_path_rand_full_numpy/masks_applied_BASE_DIFF_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.npy 

    # constructing RAW data based trainset (RANDOM indexes+ Initial Indexes)
   
    python3 fits_2_png16.py ${input_path}trainset/orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits temp3.png temp3 264 264
    mv ./temp3.png $tf_raw_input_glob_path_rand_full_png16/orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png
    mv ./temp3.npy $tf_raw_input_glob_path_rand_full_numpy/orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.npy
      
    python3 fits_2_png16.py ${input_path}trainset/masks_applied_orig_data_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits temp4.png temp4 264 264
    mv ./temp4.png $tf_raw_output_glob_path_rand_full_png16/masks_applied_orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png
    mv ./temp4.npy $tf_raw_output_glob_path_rand_full_numpy/masks_applied_orig_data_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.npy

    #constructing masks - based trainset (RANDOM indexes + Initial Indexes)

    python3 fits_2_png16.py ${input_path}visualize/masks_t_$timestep_str_3d-?_????-??-?????:??:??.???.fits temp5.png temp5 264 264 
    mv ./temp5.png $tf_masks_output_glob_path_rand_full_png16/masks_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.png
    mv ./temp5.npy $tf_masks_output_glob_path_rand_full_numpy/masks_t_${rand_timestep_str_8d}_${wavetrack_code}_${timestep_str_3d}.npy
   
    rm ./temp*   
    
    declare -i timestep_num=$timestep_num+$timestep_interval
    #declare -i pipeline_timestep=$pipeline_timestep+$1
    ((pipeline_timestep++))

done



