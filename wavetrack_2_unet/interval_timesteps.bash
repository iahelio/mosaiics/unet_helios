#!/bin/bash

echo "Wavetrack: The script to select Wavetrack output timesteps with a certain interval. Oleg Stepanyuk, 2021"
echo "Enter wavetrack output path"
read input_path
echo "Enter first timestep: "
read timestep_num_start
echo "Enter last timestep:"
read timestep_num_end 
echo "Enter timestep interval:"
read timestep_interval
echo "Enter output path for chosen timesteps (filenames remain intact):"
read output_path


# Local paths of wavetrack output and tensorflow trainsets 

vizualize_loc_path="selected_visualize/"
trainset_loc_path="selected_trainset/"
output_loc_path="selected_output/"

# Global paths for selected wavetrack output 
vizualize_glob_path="$output_path$vizualize_loc_path"
trainset_glob_path="$output_path$trainset_loc_path"
output_glob_path="$output_path$output_loc_path"

rm -r $vizualize_glob_path
rm -r $trainset_glob_path
rm -r $output_glob_path

mkdir $vizualize_glob_path
mkdir $trainset_glob_path
mkdir $output_glob_path


timestep_num=$timestep_num_start


while [ $timestep_num -lt $timestep_num_end ] 
do 

    echo "Processing" $timestep_num "timestep"

    timestep_str_2d=$( printf '%02d' $timestep_num )
    timestep_str_3d=$( printf '%03d' $timestep_num )
    
    random_timestep_str_5d=$( printf '%05d' $random_timestep_num )

    #copying selected wavetrack timesteps from vizualize, trainset and output folders    
    cp ${input_path}visualize/*_t_$timestep_str_3d* $vizualize_glob_path
    cp ${input_path}trainset/*_t_$timestep_str_3d*  $trainset_glob_path
    
    #different Wavetrack versions could have %02d or %03d timestep notation
    cp ${input_path}output_log/out_t_$timestep_str_2d* $output_glob_path >> null
    cp ${input_path}output_log/out_t_$timestep_str_3d* $output_glob_path >> null

    declare -i timestep_num=$timestep_num+$timestep_interval

done



