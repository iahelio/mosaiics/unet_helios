# Fits to 16-bit grayscale png conversion module from Wavetrack package. 
# Initial problem:  matplotlib.pyplot.imsave function saves only 8 bit grayscale images 
# while wider dynamic range is required
import numpy as np
from skimage import io, exposure, img_as_uint, img_as_float
import cv2
import sys
import pyfits

#import mahotas

def fits_2_png16_single(input_fname, output_fname, size_x, size_y):
    
    io.use_plugin('freeimage')
    full_data = pyfits.open(input_fname)
    # Load table data as tbdata 
    img_arr = full_data[1].data
    #im = np.array([[1., 2.], [3., 4.]], dtype='float64') 
    img_arr = exposure.rescale_intensity(img_arr, out_range='float')
    img_arr = img_as_uint(img_arr)
    img_arr = cv2.resize(img_arr, dsize=(self.preproc_config.x_size, self.preproc_config.y_size), interpolation=cv2.INTER_CUBIC) 
    io.imsave(output_fname,img_arr)


def fits_2_png16_single_cv2(input_fname, output_fname, size_x, size_y):
    
    print(input_fname)
    full_data = pyfits.open(input_fname)
    full_data.info()
    # Load table data as tbdata 
    img_arr = full_data[0].data
    #im = np.array([[1., 2.], [3., 4.]], dtype='float64') 
    img_arr = exposure.rescale_intensity(img_arr, out_range='float')
    img_arr = img_as_uint(img_arr)
    img_arr = cv2.resize(img_arr, dsize=(size_x, size_y), interpolation=cv2.INTER_CUBIC) 
    cv2.imwrite(output_fname, img_arr, [cv2.IMWRITE_PNG_COMPRESSION, 0]) #  No compression 



def fits_2_data(input_fname, output_fname, size_x, size_y):
    
    full_data = pyfits.open(input_fname)
    # Load table data as tbdata 
    img_arr = full_data[0].data
    #im = np.array([[1., 2.], [3., 4.]], dtype='float64') 
    img_arr = exposure.rescale_intensity(img_arr, out_range='float')
    img_arr = img_as_uint(img_arr)
    img_arr = cv2.resize(img_arr, dsize=(size_x, size_y), interpolation=cv2.INTER_CUBIC) 
    np.save(output_fname, img_arr, allow_pickle='false', fix_imports='false')


print('Fits to 16-bit grayscale png conversion module from Wavetrack package. By Oleg Stepanyuk 2021')

if ((len(sys.argv)-1) < 5):
    print ('Syntax: python3 fits_2_png16.py <input_file.fits> <output_file.png> <output_file.npy> <size_x> <size_y>')
else: 
    fits_2_png16_single_cv2(sys.argv[1],sys.argv[2],int(sys.argv[4]),int(sys.argv[5])) 
    fits_2_data(sys.argv[1],sys.argv[3],int(sys.argv[4]),int(sys.argv[5]))
